const Product = require('../models/product');

async function getLaptops({page , limit}) {
    const skip = (page - 1) * limit;
    const laptops = await Product.find({
        type:'Laptop'
    })
    .skip(skip)
    .limit(limit);

    let totalPages =   Math.ceil(await Product.find({
        type:'Laptop'
    }).count() / limit);

    return {laptops, totalPages};
  }
async function searchLaptops({q,page , limit}) {
    const regex = new RegExp(q, 'i'); // Create a case-insensitive regular expression
    const skip = (page - 1) * limit;
    const laptops = await Product.find({ name: regex })
    .skip(skip)
    .limit(limit);
    let totalPages =   Math.ceil(await Product.find({ name: regex }).count()/limit); 

    return {laptops, totalPages};
  }

module.exports = {
  getLaptops,
  searchLaptops,
};
