const User = require('../models/user');

async function createUser(username, dob, password) {
  // Check if the user already exists
  const existingUser = await User.findOne({ username });
  if (existingUser) {
    throw new Error('User already exists');
  }

  // Create a new user
  const newUser = new User({ username, dob, password });
  await newUser.save();

  return newUser;
}

async function findUserByUsername(username) {
  return User.findOne({ username });
}

module.exports = {
  createUser,
  findUserByUsername,
};
