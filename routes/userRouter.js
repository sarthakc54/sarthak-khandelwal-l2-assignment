const express = require('express');
const { authorize } = require('../utils/authMiddleware');

// User routes
const userRouter = express.Router();

const userController = require('../controllers/userController');

// Signup route endpout
userRouter.post('/signup', userController.signup);

// Signin routr endpoint
userRouter.post('/signin', userController.signin);

// This route is for testing RBAC 
userRouter.get('/user', authorize(['admin']), userController.getUser); // Protected route accessible only to 'admin' role


module.exports = userRouter;