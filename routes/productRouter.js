const express = require('express');
const { authenticateToken } = require('../utils/authMiddleware'); 

// User routes
const productRouter = express.Router();

const productController = require('../controllers/productController');

// 
productRouter.get('/laptops', authenticateToken, productController.getLaptops);
productRouter.get('/laptops/search', authenticateToken, productController.searchLaptops); // New endpoint for searching laptops


module.exports = productRouter;