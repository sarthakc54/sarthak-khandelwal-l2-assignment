const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const userRouter = require('./routes/userRouter');
const productRouter = require('./routes/productRouter');
const app = express();
const helmet = require('helmet');
const cors = require('cors');
require('dotenv').config();
app.use(cors())
app.use(bodyParser.json());

app.use(helmet());

const mongodbURI = process.env.MONGODB_URI;

// Connect to MongoDB
mongoose
  .connect(mongodbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Connected to MongoDB');
  })
  .catch((error) => {
    console.error('Error connecting to MongoDB:', error);
    process.exit(1);
  });


// Mount the user router
app.use('/api/auth', userRouter);


// Mount the product router
app.use('/api/products', productRouter);

// Start the server
const port = 3000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
