# Sample Application

This is a sample application that demonstrates a basic REST API for managing laptop products. It includes user authentication using JWT tokens and utilizes MongoDB for data storage.

## Prerequisites

Before running the application, ensure you have the following prerequisites installed:

- Node.js (version X.X.X)
- MongoDB (version X.X.X)

## Getting Started

1. Clone the repository:

   ```bash
   git clone <repository-url>
   ```

2. Install the dependencies:

   ```bash
   cd sample-application
   npm install
   ```

3. Set up the environment variables:
 NOTE: .env is already created and mongo connection is working
   - Create a new file named `.env` in the project root directory.
   - Add the following lines to the `.env` file:

     ```plaintext
     MONGODB_URI=<your-mongodb-connection-string>
     JWT_SECRET=<your-secret-key>
     ```

     Replace `<your-mongodb-connection-string>` with your actual MongoDB connection string, and `<your-secret-key>` with your desired secret key for JWT token generation and verification.

4. Start the application:

   ```bash
   npm start
   ```

5. The application should now be running locally at `http://localhost:3000`.


## Usage

- To access the endpoints that require authentication, you need to obtain a JWT token. Send a POST request to `http://localhost:3000/api/auth/signin` with the following JSON payload:

  ```json
  {
    "username": "your-email@example.com",
    "password": "your-password"
  }
  ```

  You will receive a JWT token in the response.

- Include the obtained token in the `Authorization` header of subsequent requests using the Bearer scheme:

  ```
  Authorization: Bearer <your-token>
  ```

- Refer to the API documentation for the list of available endpoints and their usage.

## License

This sample application is released under the [MIT License](LICENSE).
```

In this README file, you can replace `<repository-url>` with the actual URL of your repository. Also, make sure to update the prerequisites section with the correct versions of Node.js and MongoDB required by your application.

Feel free to customize the README file further with additional information or sections specific to your application.

Note: Ensure that the MongoDB connection string and secret key are properly set up and secured in the `.env` file, as mentioned in the instructions.