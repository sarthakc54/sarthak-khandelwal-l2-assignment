const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name: { type: String, required: true },
  type: { type: String, required: true },
  manufacture_date: { type: Date, required: true },
  // Add more fields as per your requirement
});

const Product = mongoose.model('Product', productSchema);

module.exports = Product;
