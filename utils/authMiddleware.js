const jwt = require('jsonwebtoken');
const { Unauthorized } = require('http-errors');
require('dotenv').config();

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  const jwtSecret = process.env.JWT_SECRET;

  if (!token) {
    throw new Unauthorized('Missing authentication token');
  }

  jwt.verify(token, jwtSecret, (err, user) => {
    if (err) {
      throw new Unauthorized('Invalid authentication token');
    }

    req.user = user;
    next();
  });
}

function authorize(roles) {
    return (req, res, next) => {
      const { role } = req.user;
  
      if (!roles.includes(role)) {
        return res.status(403).json({ error: 'Unauthorized' });
      }
  
      next();
    };
  }
  
  module.exports = {
    authorize,
    authenticateToken
  };
  
