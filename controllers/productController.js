const Product = require('../models/product')
const productService = require('../services/productService');

function getLaptops(req, res) {
//   const laptops = [
//     { name: 'Apple', type: 'Laptop', manufacture_date: new Date('2022-01-01') },
//     { name: 'HP', type: 'Laptop', manufacture_date: new Date('2022-02-01') },
//     { name: 'Lenovo', type: 'Laptop', manufacture_date: new Date('2022-03-01') },
//   ];

//   // Create and save the laptop products in the database
//   laptops.forEach((laptop) => {
//     const product = new Product(laptop);
//     product.save();
//   });

  // Retrieve and return the laptop products from the database
  const query = req.query;
 productService
 .getLaptops(query)
    .then(({laptops, totalPages}) => {
      res.status(200).json({ laptops , totalPages});
    })
    .catch((error) => {
      res.status(500).json({ error: 'An error occurred while fetching laptop products' });
    });
}
async function searchLaptops(req, res) {
    const query = req.query; // Get the query parameter from the request URL
  
    if (!query) {
      return res.status(400).json({ error: 'Missing search query' });
    }
  
    const {laptops, totalPages} = await productService.searchLaptops(query);
    res.status(200).json({  laptops , totalPages });
  }

module.exports = {
    getLaptops,
    searchLaptops,
};
