const jwt = require('jsonwebtoken');
const userService = require('../services/userService');
require('dotenv').config();
const jwtSecret = process.env.JWT_SECRET;
const { authorize } = require('../utils/authMiddleware');


function signup(req, res) {
//console.log("Inside Signup")
  const { username, dob , password } = req.body;

  // Validate request data
  if (!username || !dob || !password) {
    return res.status(400).json({ error: 'Missing required fields' });
  }

  userService
    .createUser(username, dob, password)
    .then((newUser) => {
      // Generate JWT token
      const token = jwt.sign({ username }, jwtSecret, { expiresIn: '1h' });

      // Return the token and the created user
      res.status(201).json({ message: 'User created successfully', token, user: newUser });
    })
    .catch((error) => {
      res.status(409).json({ error: error.message });
    });
}

function signin(req, res) {
    console.log("INSIDE SIGIN")
  const { username, password } = req.body;

  // Validate request data
  if (!username || !password) {
    return res.status(400).json({ error: 'Missing required fields' });
  }

  userService
    .findUserByUsername(username)
    .then((user) => {
      if (!user || user.password !== password) {
        return res.status(401).json({ error: 'Invalid credentials' });
      }

      // Generate JWT token
      const token = jwt.sign({ username }, jwtSecret, { expiresIn: '1h' });

      // Return the token and the user
      res.status(200).json({ message: 'User signed in successfully', token, user });
    })
    .catch((error) => {
      res.status(500).json({ error: error.message });
    });
}

function getUser(req, res) {
    // Return the user details
    res.status(200).json({ user: req.user });
  }
module.exports = {
  signup,
  signin,
  getUser,
};
